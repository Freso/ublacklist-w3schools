<!--
SPDX-FileCopyrightText: © 2024 Frederik “Freso” S. Olesen

SPDX-License-Identifier: CC-BY-SA-4.0
-->
# ublacklist-w3schools

Subscription list for [uBlacklist](https://iorate.github.io/ublacklist/)
to hide W3Schools from search results.

## Usage

Add [`ublacklist-w3schools.txt`](https://codeberg.org/Freso/ublacklist-w3schools/raw/branch/main/ublacklist-w3schools.txt)
to your uBlacklist subscriptions:
```
https://codeberg.org/Freso/ublacklist-w3schools/raw/branch/main/ublacklist-w3schools.txt
```

See [uBlacklist’s documentation on adding and handling subscriptions](https://iorate.github.io/ublacklist/docs/advanced-features#subscription)
for general instructions in how to handle subscriptions in uBlacklist.

## Ancestry

This list was inspired by [Stefano Nardo’s “Hide W3Schools”](https://addons.mozilla.org/firefox/addon/hide-w3schools/)
and [Inan Hira’s “Hide w3schools from DuckDuckGo”](https://git.sr.ht/~eih/hide-w3schools-ddg)
browser extensions/addons. I didn’t want to install yet another browser
extension for this singular purpose though, so I decided to instead make a list
for the general purpose [uBlacklist extension](https://iorate.github.io/ublacklist/).

## License

[The list itself](ublacklist-w3schools.txt) is dedicated to the public domain
using the [Creative Commons CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
legal tool.

[This README](README.md) is licensed under the
[Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
license.

Both licenses are included for your reference in the
[`LICENSES`](LICENSES) directory.
Additionally the licenses for both of these files, and any files that might be
added in the future, are documented using the
[REUSE Specification](https://reuse.software/spec/).
